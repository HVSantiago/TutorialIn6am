import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../../../services/usuario.service';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styles: []
})
export class UsuarioComponent implements OnInit {
  usuario:any[] = [];
  constructor(
    private usuarioService:UsuarioService
  ) { }

  public inicializar(){
    this.usuarioService.getUsuario().subscribe(data => {
      this.usuario = data;
    });
  }

  ngOnInit() {
   this.inicializar();
  }

}
